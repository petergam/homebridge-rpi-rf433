var RF433 = require('rpi-rf433')
var Service, Characteristic

module.exports = function(homebridge) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  homebridge.registerAccessory("homebridge-rpi-rf433", "RPi-RF433", RF433Accessory)
}

var RF433Accessory = (function () {
  function RF433Accessory(log, config) {
    var that = this
    this.config = config
    this.log = log
    this.isPowerOn = false

    this.getPowerState = function (callback) {
      that.log('Power state for ' + that.config.name + ' is ' + that.isPowerOn);
      callback(null, that.isPowerOn);
    };

    this.setPowerState = function (isPowerOn, callback) {
      that.isPowerOn = isPowerOn
      that.log("Turning " + that.config.name + " " + (that.isPowerOn == true ? "on" : "off"))
      var code = that.isPowerOn ? that.config.rf_on_code : that.config.rf_off_code
      var protocol = that.config.protocol

      var sendCode = function () {
        RF433.sendCode(code, { protocol: protocol }, function (error, stdout) {
          that.log(error ? 'Failed: ' + error : 'Sent ' + stdout)
        })
      }

      sendCode()

      callback(null)
    }
    
    this.identify = function (callback) {
      that.log(that.config.name + " was identified.")
      callback()
    }

    this.getServices = function () {
      var outletService;
      if (that.config.type == "Light") {
          outletService = new Service.Lightbulb(that.config.name);
          outletService.getCharacteristic(Characteristic.On)
              .on('get', that.getPowerState.bind(that))
              .on('set', that.setPowerState.bind(that));
      } else if (that.config.type == "Switch") {
        outletService = new Service.Switch(that.config.name);
        outletService.getCharacteristic(Characteristic.On)
            .on('get', that.getPowerState.bind(that))
            .on('set', that.setPowerState.bind(that));
      }
      var informationService = new Service.AccessoryInformation()
          .setCharacteristic(Characteristic.Manufacturer, that.config.manufacturer)
          .setCharacteristic(Characteristic.Model, that.config.model)
          .setCharacteristic(Characteristic.SerialNumber, that.config.serial);
      return [informationService, outletService];
    }
  }
  
  return RF433Accessory
}())
